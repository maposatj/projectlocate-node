var tz = "Africa/Johannesburg";
require('./log')(tz);
var moment = require('moment-timezone');
var http = require('http');
var express = require("express");
var bodyParser = require('body-parser');
var cors = require('cors');
var yargs = require('yargs').argv;
var nodemailer = require('nodemailer');
moment.tz.setDefault(tz);
var app = module.exports.app = express();
app.use(cors());
app.options('*', cors());
var server = http.createServer(app);
var io = require('socket.io')(server);

var router = express.Router();
app.use(bodyParser.urlencoded({extended: false}))

// parse application/json
app.use(bodyParser.json());
app.use("/api/", router);

var sockets = [];
var trackedDevices = [];
var sendEmailOnTrackDeviceConnect = true;
if (yargs.sendEmailOnTrackDeviceConnect) {
    sendEmailOnTrackDeviceConnect = yargs.sendEmailOnTrackDeviceConnect;
}

io.origins('*:*');
io.on('connection', function(client) {
    client.isTrackedDevice = checkIsTrackedDevice(client.request.headers['user-agent']);
    if (client.isTrackedDevice === true) {
        trackedDevices.push(client);
        console.log('Tracked Device Client connected');
        sendNotification(client);
    } else {
        console.log('Client connected');
    }
    client.on('uuid', function(data) {
        client.uuid = data.uuid;
        sockets.push(client);
    });
    client.on('disconnect', function() {
        var isTrackedDevice = client.isTrackedDevice;
        if (isTrackedDevice == true) {
            var index = trackedDevices.indexOf(client);
            if (index >= 0) {
                trackedDevices.splice(index, 1);
            }
        } else {
            var index = sockets.indexOf(client);
            if (index >= 0) {
                sockets.splice(index, 1);
            }
        }
        console.log('Client Disconnected, isTrackedDevice: ' + isTrackedDevice);
    });
    client.on('cMessage', function(d) {
        console.log('cMessage: ' + JSON.stringify(d));
        if (sockets.length > 0) {
            for (var i in sockets) {
                var socket = sockets[i];
                if (socket.connected) {
                    socket.emit('cMessageResponse', d);
                }
            }
        }
    });
});

router.post('/send-command', function(req, res) {
    console.log('path: ' + req.path + ', method: ' + req.method);
    var socket = findSocketByUUID(req.body.uuid);
    if (trackedDevices.length > 0) {
        // Get gps by default
        var command = 1;
        var commandText = req.body.command;
        if (commandText) {
            if (commandText == 'enable-gps') {
                command = 2;
            }
            if (commandText == 'disable-gps') {
                command = 3;
            }
        }
        console.log('command: ' + command);
        for (var k in trackedDevices) {
            var cDeviceSocket = trackedDevices[k];
            cDeviceSocket.emit('command', JSON.stringify({
                'command': command,
                'data': {}
            }));
        }
    }
    res.json({message: 'Command received'});
});

function findSocketByUUID(uuid) {
    for (var k in sockets) {
        var obj = sockets[k];
        if (obj.uuid = uuid) {
            return obj;
        }
    }
    return null;
}

function checkIsTrackedDevice(userAgent) {
    return userAgent.match(/GT-S7580/i) ? true : false;
}

function sendNotification(socket) {
    if (!sendEmailOnTrackDeviceConnect) {
        return;
    }
    var ip = socket.request.connection.remoteAddress;
    var userAgent = socket.request.headers['user-agent'];

    var mailPwd = yargs.mailerPassword;
    var smtpConfig = 'smtps://maposa.takalani%40gmail.com:' + mailPwd + '@smtp.gmail.com';
    var transporter = nodemailer.createTransport(smtpConfig);
    var recepients = ['maposa.takalani+device.connected@gmail.com'];

    var text = JSON.stringify({
        ip: ip,
        userAgent: userAgent,
        timestamp: moment().format("YYYY-MM-DD HH:mm:ss")
    }, undefined, 4);
    text = syntaxHighlight(text);

    var opt = {
        sender: 'maposa.takalani@gmail.com',
        to: recepients.join(", "),
        subject: 'Device connected notification',
        html: '<html><head><style type="text/css">pre {outline: 1px solid #ccc; padding: 5px; margin: 5px; }.string { color: green; }.number { color: darkorange; }.boolean { color: blue; }.null { color: magenta; }.key { color: red; }</style></head><body><pre>' + text + '</pre></body></html>'
    };
    transporter.sendMail(opt, function(error, info) {
        if (error) {
            return console.log(error);
        }
        console.log('Message sent: ' + info.response);
    });
}

function syntaxHighlight(json) {
    if (typeof json != 'string') {
        json = JSON.stringify(json, undefined, 2);
    }
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function(match) {
        var cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '"><small>' + match + '</small></span>';
    });
}

server.listen(3005, 'localhost', function(er) {
    if (er) {
        console.log(er);
    }
    console.log('Running');
});
