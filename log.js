var winston = require('winston');
var moment = require('moment-timezone');
var fs = require('fs');
var env = process.env.NODE_ENV || 'development';
var logDir = 'log';
if (!fs.existsSync(logDir)) {
    fs.mkdirSync(logDir);
}

function initLogger(logDir, env) {
    var logger = new (winston.Logger)({
        transports: [
            // colorize the output to the console
            new (winston.transports.Console)({
                timestamp: function() {
                    return moment().format("YYYY-MM-DD HH:mm:ss");
                },
                colorize: true,
            }),
            new (winston.transports.File)({
                filename: logDir + '/log.log',
                timestamp: function() {
                    return moment().format("YYYY-MM-DD HH:mm:ss");
                },
                level: env === 'development' ? 'debug' : 'info'
            })
        ]
    });
    logger.level = 'debug';
    return logger;
}

module.exports = function decorateLog(timezone) {
    moment.tz.setDefault(timezone);
    var logger = initLogger(logDir, env);
    var originalFunc = console.log;
    console.log = function() {
        logger.debug(arguments[0]);
        originalFunc.apply(console, ["log:"].concat([].slice.call(arguments)));
    };
};